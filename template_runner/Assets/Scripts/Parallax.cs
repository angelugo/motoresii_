﻿using UnityEngine;

public class Parallax : MonoBehaviour
{
    public GameObject cam;
    private float length, startPos;
    public float parallaxEffect;
    public Controller_Hud GameOver;

    void Start()
    {
       
        GameOver = GameObject.FindObjectOfType<Controller_Hud>();//Esta Linea lo que hace es bucar un objeto con el script"Controller_Hud" y referenciarlo en el inspector
        startPos = transform.position.x;//Settea la variable startPos y la iguala a transform.position haciendo referencia a la position del gameObject del objeto que tenga este codigo
        length = GetComponent<SpriteRenderer>().bounds.size.x;//Sette la variable  length y la igualamos al componenete SpriteRenderer en su propiedad bounds.size.x, esto guarda el tamaño del limite en la coordenada x
    }

    void Update()
    {
        transform.position = new Vector3(transform.position.x - parallaxEffect, transform.position.y, transform.position.z);//Igualamos el transform.position del gameObject que tenga este codigo a una instancia de new vecotr3 que  y movemos su eje x a -ParallaxEffect, en los demos ejes lo dejamos iugual
        if (transform.localPosition.x < -20)//Usamos la sentencia if para preguntar si el transform.localPosition en el eje x es < a -20
        {
            transform.localPosition = new Vector3(20, transform.localPosition.y, transform.localPosition.z);//Si la condicion se cumple igualamos el transform.LocalPosition del gameObject que tenga este codigo a na nueva instnacia de vector3 indicando que en el eje del vector2 sea 20, los demas ejes los dejamos iguales
        }
        if (GameOver.gameOver == true)//Hace uso de la entencia "if" pregutando si la propidead gameOver de la variable gameOver es true
        {
            parallaxEffect = 0;// Si se cumple la condicion igualamos la variable parallaxEffect a 0
        }
    }
}
