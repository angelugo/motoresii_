﻿using UnityEngine;

public class Controller_Enemy : MonoBehaviour
{
    public static float enemyVelocity;
    private Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();//Optiene componente "Rigidbodody" y se lo asigna a la variable "rb"
    }

    void Update()
    {
        rb.AddForce(new Vector3(-enemyVelocity, 0, 0), ForceMode.Force);// se hace uso "AddForce" para aplicar una fuerza al rb que es un rigidbody, y se instancia un un vector 3 para aplicar una fuerza determinada(enemyVelocity) en la x negativa generando que el prefab que tenga este codigo avance en -x
        OutOfBounds();//Se llama a la funcion "OutOfBounds"
    }

    public void OutOfBounds()//declaramo una funcion llamada "OutOfBonuds"
    {
        if (this.transform.position.x <= -15)//Hace uso de la sentencia "if" indicando que en caso de que this.(Haciendo referencia a quien tiene este codigo ) en su transform.position.x <=-15 que hace referencia a la posicion -15 en el eje x
        {
            Destroy(this.gameObject);//Hace uso del metodo "Destroy" para destruir a "this.gameObject" que se refiere al game object de quien tenga este codigo
        }
    }
}
