﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BarraDeProgreso : MonoBehaviour
{

    public float valor = 100;
    public Image BarraDevida;
    public float tiempo;
    public Controller_Hud gameOver;

    private void Start()
    {
        gameOver = GameObject.FindObjectOfType<Controller_Hud>();//Esta Linea lo que hace es bucar un objeto con el script"Controller_Hud" y referenciarlo en el inspector
    }
    void Update()
    {
       
        BarraDevida.fillAmount = valor / 100;//Hacemos uso de la proiedad fillAmount de nuestra imagen y dividimos la cantidad de la variable "Valor" por 100 normalizando la barra  
        if (gameOver.gameOver == false)// usamos la sentencia "if" para preguntar si la propiedad gameOver de la variable gameOver es falsa
        {
         
                StartCoroutine(Reducir());// invocamos una corrutina llamada "Reducir"

            
          
          
          
        }
        if (valor <= 0.0f)//Hacemos uso de la sentencia "if" para preguntar si la variable calor es igual o menor de 0.0f
        {
            gameOver.gameOver = true;// si la sentencia "if" se cumple  la propiedad "gameOver" de la variable "gameOver" es true
        }
       
    }
    IEnumerator Reducir()//Definimos un metodo IEnumerator
    {
        
        yield return new WaitForSeconds(tiempo);//Indicamos que espere segundos segun la cantidad de la variable "tiempo"
        valor -= Time.deltaTime * 3;// luego de que espere  los segundos indicados igualamos a la resta de valor y time.deltaTime * 3 lo cual aumenta el decremento

    }
}
