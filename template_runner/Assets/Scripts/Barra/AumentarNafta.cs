﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AumentarNafta : MonoBehaviour
{
    public BarraDeProgreso Valor;//Definimos una variable Publica te tipo BarraDeprogreso llamada valor 
    public float aumento;//Definimos una variable Publica de tipo float llamada aumento que represente el valor de aumento 
   
    void Start()
    {
        Valor = GameObject.FindObjectOfType<BarraDeProgreso>();//Esta Linea lo que hace es bucar un objeto con el script"BarraDeProgreso" y referenciarlo en el inspector
    }


    private void OnTriggerEnter(Collider other)//Hacemos uso del metodo OntriggerEnter para disparar un trigger y sobrecargamos la metodo con una variable de tipo Collider llamada Other que representa el otro objeto con el que hacemos colision
    {
        if (other.gameObject.CompareTag("Nafta"))//Hacemos uso de la sentencia "if" y preguntamos  si other.gameObject.CompareTag("Nafta"), osea si el otro gameObject con el hacemos colision tiene la tga"Nafta"
        {
            Valor.valor += aumento;//Si se cumple la condicion entonce igualamos a la propiedad valor de la variable valor igual a la suma de de valor.valor y aumento 
            Destroy(other.gameObject);//Usamos el metodo "Destroy" para destruir al a other.gameObject, que hace referencia al obejeto con el que disparamos la trigger 
        }
    }


}
