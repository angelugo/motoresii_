﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Disparar : MonoBehaviour
{
    public float rango;
    public GameObject origen;
  
    
    public GameObject proyectil;
    void Update()
    {
    

        if (Input.GetKeyDown(KeyCode.W))// Usa la sentencia "if" preguntando si Input.GetKeyDown(KeyCode.W) es accionado 
        {

            RaycastHit hit;// se define una variable local tipo raycastHit llamada "hit"
            if (Physics.Raycast(origen.transform.position, origen.transform.forward, out hit, rango))//En esta linea detectamos es raycasting, usamos Physics.Raycast() para generar el rayo dando el punto inicial, una direccion, luego el parametro de salida "Out" que optine la informacion que guarmamos en "hit, y por ultimo el rango"
            {

                GameObject refe = Instantiate(proyectil, hit.point, Quaternion.identity);//hacemos uso de una varialbe local para guardar la informacion del objeto que queremos instanciar
                Destroy(refe,0.1f);//Hacemos uso del metodo "Destroy" para destruir la variable local refe en 0.1f segundos
                
            }
        }
    }
    private void OnDrawGizmos()//Usamos "OnDrawGizmos" para ver en el en la escena los los gizmos
    {
        Gizmos.color = Color.red;//Espesificamos el color de los Gizmos usando Gizmos.color = Color.red, generando un color rojo en el gizmos
        Gizmos.DrawRay(origen.transform.position, origen.transform.forward * rango);// Llamamos el metodo DrawRay para dibujar un rayo tomando un punto de origen(origen.transform.position), una direccion(origen.transform.forward) y un rango(Rango) 
    }
}
