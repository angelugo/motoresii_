﻿using UnityEngine;
using UnityEngine.UI;

public class Controller_Hud : MonoBehaviour
{
    public  bool gameOver = false;
    public Text distanceText;
    public Text gameOverText;
    private float distance = 0;

    void Start()
    {
        gameOver = false;//Setteamos la variable"gameOver" en falso 
        distance = 0;//Sttea el valor a distance en 0
        distanceText.text = distance.ToString("f0");//Aca iguala  la variable "Text" de  la Hud con el numero de la Distancia, usamos el .ToString para que el .text lo pueda leer
        gameOverText.gameObject.SetActive(false);//Aca settea el letrero de "GameOver", se usa el .gameObject para ocultar el letreto 
       
    }

    void Update()
    {
        if (gameOver)//Se hace uso de la sentencia "if" para hacer una condicional y preguntar si "gameOver"(Bool) es "True"
        {
            Time.timeScale = 0;// detiene el tiempo del juego y se pausa el juego 
            
            gameOverText.text = "Game Over \n Total Distance: " + distance.ToString("f0");//Nos muestra la distancia final que realizamos en el juego, hacemos uso de el ultimo valor que toma la distancia
            gameOverText.gameObject.SetActive(true);// hacemos uso del metodo ".gameObject.SetActive(true)" para mostrar el tablero de "gameOver"
        }
        else//Usamos "else" para la condicional y preguntar que en caso de no complir con el (if(gameOver)) entoces ejecutar lo siguiente
        {
            distance += Time.deltaTime;//Igualamos a la suma de la variable distancia y Time.deltaTime asi asignado el valor del "deltatime" a la distancia
            
            distanceText.text = distance.ToString("f0");// igualamos la variable "distanceText.text" con el valor actual de distance, usamos .ToString(f0) para que el .text puede leer el valor de distance  y el (f0) es para redondear le valor decimal que optiene de time.deltaTime
           
        }
    }
}
